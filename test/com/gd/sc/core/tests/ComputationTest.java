package com.gd.sc.core.tests;

import static org.junit.Assert.*;

import org.junit.Test;

import com.gd.sc.core.computation.impl.BasicMathImpl;

public class ComputationTest {

	@Test
	public void testAddition() {
		//Test that addition method performs it's function properly
		BasicMathImpl basicMath = new BasicMathImpl();
		
		double result = basicMath.add(17, 34);
		
		if(result == 51){
			System.out.println("Addition if performed as it should be");
		}
		else{
			System.out.println("All you had to do was put a plus sign. How hard can it be?");
		}
	}
	
	@Test
	public void testSubtraction() {
		//Test that addition method performs it's function properly
		BasicMathImpl basicMath = new BasicMathImpl();
		
		double result = basicMath.subtract(17, 34);
		
		if(result == -17){
			System.out.println("Subtraction if performed as it should be");
		}
		else{
			System.out.println("All you had to do was put a minus sign. How hard can it be?");
		}
	}
	
	@Test
	public void testMultiplication() {
		//Test that addition method performs it's function properly
		BasicMathImpl basicMath = new BasicMathImpl();
		
		double result = basicMath.multiply(17, 34);
		
		if(result == 578){
			System.out.println("Multpilcation if performed as it should be");
		}
		else{
			System.out.println("All you had to do was put a * sign. How hard can it be?");
		}
	}
	
	@Test
	public void testdivsion() {
		//Test that addition method performs it's function properly
		BasicMathImpl basicMath = new BasicMathImpl();
		
		double result = basicMath.divide(17, 34);
		
		System.out.println(result);
		
		if(result == 0.5){
			System.out.println("Division if performed as it should be");
		}
		else{
			System.out.println("All you had to do was put a / sign. How hard can it be?");
		}
	}

}
