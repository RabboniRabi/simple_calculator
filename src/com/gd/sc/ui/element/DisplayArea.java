package com.gd.sc.ui.element;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.gd.sc.controller.CalculatorSingleton;
import com.gd.sc.utils.Constant;

public class DisplayArea extends JPanel {
	private JLabel historyField;
	private JLabel operatorField;
	private JLabel inputField;

	public DisplayArea() {
		historyField = new JLabel();
		operatorField = new JLabel();
		inputField = new JLabel();

		GroupLayout layout = new GroupLayout(this);
		layout.setHorizontalGroup(layout.createParallelGroup().addComponent(historyField, Constant.HISTORY_WIDTH, Constant.HISTORY_WIDTH, Constant.HISTORY_WIDTH).addGroup(layout.createSequentialGroup().addComponent(operatorField, Constant.OPERATOR_WIDTH, Constant.OPERATOR_WIDTH, Constant.OPERATOR_WIDTH).addComponent(inputField, Constant.INPUT_WIDTH, Constant.INPUT_WIDTH, Constant.INPUT_WIDTH)));
		layout.setVerticalGroup(layout.createSequentialGroup().addComponent(historyField, Constant.HISTORY_HEIGHT, Constant.HISTORY_HEIGHT, Constant.HISTORY_HEIGHT).addGroup(layout.createParallelGroup().addComponent(operatorField, Constant.OPERATOR_HEIGHT, Constant.OPERATOR_HEIGHT, Constant.OPERATOR_HEIGHT).addComponent(inputField, Constant.INPUT_HEIGHT, Constant.INPUT_HEIGHT, Constant.INPUT_HEIGHT)));

		setLayout(layout);
	}

	public void update() {
		historyField.setText(CalculatorSingleton.getInstanceof().getHistory());
		operatorField.setText(CalculatorSingleton.getInstanceof().getOperator().toString());
		inputField.setText(CalculatorSingleton.getInstanceof().getInput().toString());
		setVisible(true);
	}
}
