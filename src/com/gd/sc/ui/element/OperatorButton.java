package com.gd.sc.ui.element;

import javax.swing.JButton;

import com.gd.sc.ui.listener.OperatorListener;
import com.gd.sc.utils.OperationEnum;

public class OperatorButton extends JButton {

	private OperationEnum operator;

	public OperatorButton(OperationEnum operator, DisplayArea area) {
		this.operator = operator;
		setText("" + operator.getOperation());
		addActionListener(new OperatorListener(operator, area));
		setVisible(true);
		setFocusable(false);
	}
}
