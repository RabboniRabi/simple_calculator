package com.gd.sc.ui.element;

import javax.swing.JButton;

import com.gd.sc.ui.listener.NumericListener;
import com.gd.sc.utils.NumberEnum;

public class NumericButton extends JButton {

	private NumberEnum number;

	public NumericButton(NumberEnum number, DisplayArea area) {
		this.number = number;
		setText("" + number.getNumber());
		addActionListener(new NumericListener(number, area));
		setVisible(true);
		setFocusable(false);
	}
}
