package com.gd.sc.ui;

import javax.swing.GroupLayout;
import javax.swing.JFrame;
import javax.swing.JPanel;

import com.gd.sc.ui.element.DisplayArea;
import com.gd.sc.ui.element.NumericButton;
import com.gd.sc.ui.element.OperatorButton;
import com.gd.sc.ui.listener.NumericKeyListener;
import com.gd.sc.utils.Constant;
import com.gd.sc.utils.NumberEnum;
import com.gd.sc.utils.OperationEnum;

public class GUI extends JFrame {
	public GUI() {
		setSize(Constant.WINDOW_WIDTH, Constant.WINDOW_HEIGHT);
		setResizable(false);
		DisplayArea display = new DisplayArea();
		NumericButton _0 = new NumericButton(NumberEnum._0, display);
		NumericButton _1 = new NumericButton(NumberEnum._1, display);
		NumericButton _2 = new NumericButton(NumberEnum._2, display);
		NumericButton _3 = new NumericButton(NumberEnum._3, display);
		NumericButton _4 = new NumericButton(NumberEnum._4, display);
		NumericButton _5 = new NumericButton(NumberEnum._5, display);
		NumericButton _6 = new NumericButton(NumberEnum._6, display);
		NumericButton _7 = new NumericButton(NumberEnum._7, display);
		NumericButton _8 = new NumericButton(NumberEnum._8, display);
		NumericButton _9 = new NumericButton(NumberEnum._9, display);

		OperatorButton ADD = new OperatorButton(OperationEnum.ADD, display);
		OperatorButton SUB = new OperatorButton(OperationEnum.SUBTRACT, display);
		OperatorButton MUL = new OperatorButton(OperationEnum.MULTIPLY, display);
		OperatorButton DIV = new OperatorButton(OperationEnum.DIVIDE, display);
		OperatorButton EQ = new OperatorButton(OperationEnum.EQUALS, display);
		OperatorButton DEC = new OperatorButton(OperationEnum.DECIMAL, display);
		OperatorButton BACK = new OperatorButton(OperationEnum.BACK, display);
		OperatorButton SIGN = new OperatorButton(OperationEnum.SIGN, display);
		OperatorButton CLEAR_HISTORY = new OperatorButton(OperationEnum.CLEAR_HISTORY, display);
		OperatorButton CLEAR_INPUT = new OperatorButton(OperationEnum.CLEAR_INPUT, display);

		JPanel numpad = new JPanel();
		GroupLayout numpadLayout = new GroupLayout(numpad);
		numpadLayout.setVerticalGroup(numpadLayout.createSequentialGroup().addGroup(numpadLayout.createParallelGroup().addComponent(_1, Constant.BUTTON_HEIGHT, Constant.BUTTON_HEIGHT, Constant.BUTTON_HEIGHT).addComponent(_2, Constant.BUTTON_HEIGHT, Constant.BUTTON_HEIGHT, Constant.BUTTON_HEIGHT).addComponent(_3, Constant.BUTTON_HEIGHT, Constant.BUTTON_HEIGHT, Constant.BUTTON_HEIGHT)).addGroup(numpadLayout.createParallelGroup().addComponent(_4, Constant.BUTTON_HEIGHT, Constant.BUTTON_HEIGHT, Constant.BUTTON_HEIGHT).addComponent(_5, Constant.BUTTON_HEIGHT, Constant.BUTTON_HEIGHT, Constant.BUTTON_HEIGHT).addComponent(_6, Constant.BUTTON_HEIGHT, Constant.BUTTON_HEIGHT, Constant.BUTTON_HEIGHT)).addGroup(numpadLayout.createParallelGroup().addComponent(_7, Constant.BUTTON_HEIGHT, Constant.BUTTON_HEIGHT, Constant.BUTTON_HEIGHT).addComponent(_8, Constant.BUTTON_HEIGHT, Constant.BUTTON_HEIGHT, Constant.BUTTON_HEIGHT).addComponent(_9, Constant.BUTTON_HEIGHT, Constant.BUTTON_HEIGHT, Constant.BUTTON_HEIGHT)).addGroup(numpadLayout.createParallelGroup().addComponent(CLEAR_HISTORY, Constant.BUTTON_HEIGHT, Constant.BUTTON_HEIGHT, Constant.BUTTON_HEIGHT).addComponent(_0, Constant.BUTTON_HEIGHT, Constant.BUTTON_HEIGHT, Constant.BUTTON_HEIGHT).addComponent(CLEAR_INPUT, Constant.BUTTON_HEIGHT, Constant.BUTTON_HEIGHT, Constant.BUTTON_HEIGHT)));
		numpadLayout.setHorizontalGroup(numpadLayout.createParallelGroup().addGroup(numpadLayout.createSequentialGroup().addComponent(_1, Constant.BUTTON_WIDTH, Constant.BUTTON_WIDTH, Constant.BUTTON_WIDTH).addComponent(_2, Constant.BUTTON_WIDTH, Constant.BUTTON_WIDTH, Constant.BUTTON_WIDTH).addComponent(_3, Constant.BUTTON_WIDTH, Constant.BUTTON_WIDTH, Constant.BUTTON_WIDTH)).addGroup(numpadLayout.createSequentialGroup().addComponent(_4, Constant.BUTTON_WIDTH, Constant.BUTTON_WIDTH, Constant.BUTTON_WIDTH).addComponent(_5, Constant.BUTTON_WIDTH, Constant.BUTTON_WIDTH, Constant.BUTTON_WIDTH).addComponent(_6, Constant.BUTTON_WIDTH, Constant.BUTTON_WIDTH, Constant.BUTTON_WIDTH)).addGroup(numpadLayout.createSequentialGroup().addComponent(_7, Constant.BUTTON_WIDTH, Constant.BUTTON_WIDTH, Constant.BUTTON_WIDTH).addComponent(_8, Constant.BUTTON_WIDTH, Constant.BUTTON_WIDTH, Constant.BUTTON_WIDTH).addComponent(_9, Constant.BUTTON_WIDTH, Constant.BUTTON_WIDTH, Constant.BUTTON_WIDTH)).addGroup(numpadLayout.createSequentialGroup().addComponent(CLEAR_HISTORY, Constant.BUTTON_WIDTH, Constant.BUTTON_WIDTH, Constant.BUTTON_WIDTH).addComponent(_0, Constant.BUTTON_WIDTH, Constant.BUTTON_WIDTH, Constant.BUTTON_WIDTH).addComponent(CLEAR_INPUT, Constant.BUTTON_WIDTH, Constant.BUTTON_WIDTH, Constant.BUTTON_WIDTH)));
		numpad.setLayout(numpadLayout);

		JPanel opPad = new JPanel();
		GroupLayout opPadLayout = new GroupLayout(opPad);
		opPadLayout.setVerticalGroup(opPadLayout.createSequentialGroup().addGroup(opPadLayout.createParallelGroup().addComponent(ADD, Constant.BUTTON_HEIGHT, Constant.BUTTON_HEIGHT, Constant.BUTTON_HEIGHT).addComponent(SUB, Constant.BUTTON_HEIGHT, Constant.BUTTON_HEIGHT, Constant.BUTTON_HEIGHT)).addGroup(opPadLayout.createParallelGroup().addComponent(MUL, Constant.BUTTON_HEIGHT, Constant.BUTTON_HEIGHT, Constant.BUTTON_HEIGHT).addComponent(DIV, Constant.BUTTON_HEIGHT, Constant.BUTTON_HEIGHT, Constant.BUTTON_HEIGHT)).addGroup(opPadLayout.createParallelGroup().addComponent(EQ, Constant.BUTTON_HEIGHT, Constant.BUTTON_HEIGHT, Constant.BUTTON_HEIGHT).addComponent(BACK, Constant.BUTTON_HEIGHT, Constant.BUTTON_HEIGHT, Constant.BUTTON_HEIGHT)).addGroup(opPadLayout.createParallelGroup().addComponent(DEC, Constant.BUTTON_HEIGHT, Constant.BUTTON_HEIGHT, Constant.BUTTON_HEIGHT).addComponent(SIGN, Constant.BUTTON_HEIGHT, Constant.BUTTON_HEIGHT, Constant.BUTTON_HEIGHT)));
		opPadLayout.setHorizontalGroup(opPadLayout.createParallelGroup().addGroup(opPadLayout.createSequentialGroup().addComponent(ADD, Constant.BUTTON_WIDTH, Constant.BUTTON_WIDTH, Constant.BUTTON_WIDTH).addComponent(SUB, Constant.BUTTON_WIDTH, Constant.BUTTON_WIDTH, Constant.BUTTON_WIDTH)).addGroup(opPadLayout.createSequentialGroup().addComponent(MUL, Constant.BUTTON_WIDTH, Constant.BUTTON_WIDTH, Constant.BUTTON_WIDTH).addComponent(DIV, Constant.BUTTON_WIDTH, Constant.BUTTON_WIDTH, Constant.BUTTON_WIDTH)).addGroup(opPadLayout.createSequentialGroup().addComponent(EQ, Constant.BUTTON_WIDTH, Constant.BUTTON_WIDTH, Constant.BUTTON_WIDTH).addComponent(BACK, Constant.BUTTON_WIDTH, Constant.BUTTON_WIDTH, Constant.BUTTON_WIDTH)).addGroup(opPadLayout.createSequentialGroup().addComponent(DEC, Constant.BUTTON_WIDTH, Constant.BUTTON_WIDTH, Constant.BUTTON_WIDTH).addComponent(SIGN, Constant.BUTTON_WIDTH, Constant.BUTTON_WIDTH, Constant.BUTTON_WIDTH)));
		opPad.setLayout(opPadLayout);

		JPanel mainPanel = new JPanel();
		GroupLayout layout = new GroupLayout(mainPanel);
		layout.setVerticalGroup(layout.createSequentialGroup().addComponent(display, Constant.DISPLAY_HEIGHT, Constant.DISPLAY_HEIGHT, Constant.DISPLAY_HEIGHT).addGroup(layout.createParallelGroup().addComponent(numpad, Constant.NUMPAD_HEIGHT, Constant.NUMPAD_HEIGHT, Constant.NUMPAD_HEIGHT).addComponent(opPad, Constant.OPPAD_HEIGHT, Constant.OPPAD_HEIGHT, Constant.OPPAD_HEIGHT)));
		layout.setHorizontalGroup(layout.createParallelGroup().addComponent(display, Constant.DISPLAY_WIDTH, Constant.DISPLAY_WIDTH, Constant.DISPLAY_WIDTH).addGroup(layout.createSequentialGroup().addComponent(numpad, Constant.NUMPAD_WIDTH, Constant.NUMPAD_WIDTH, Constant.NUMPAD_WIDTH).addComponent(opPad, Constant.OPPAD_WIDTH, Constant.OPPAD_WIDTH, Constant.OPPAD_WIDTH)));
		mainPanel.setLayout(layout);
		mainPanel.setFocusable(true);
		mainPanel.addKeyListener(new NumericKeyListener(display));

		add(mainPanel);
		setVisible(true);
		setTitle(Constant.TITLE);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	public static void main(String[] args) {
		new GUI();
	}
}
