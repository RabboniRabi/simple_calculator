package com.gd.sc.ui.listener;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import com.gd.sc.controller.CalculatorSingleton;
import com.gd.sc.ui.element.DisplayArea;
import com.gd.sc.utils.NumberEnum;
import com.gd.sc.utils.OperationEnum;

public class NumericKeyListener implements KeyListener {

	private DisplayArea area;

	public NumericKeyListener(DisplayArea area) {
		this.area = area;
	}

	@Override
	public void keyTyped(KeyEvent e) {
	}

	@Override
	public void keyPressed(KeyEvent e) {
	}

	@Override
	public void keyReleased(KeyEvent e) {
		NumberEnum number = null;
		OperationEnum operation = null;
		switch (e.getKeyCode()) {
		case 8: {
			operation = OperationEnum.BACK;
		}
			break;
		case 10: {
			operation = OperationEnum.EQUALS;
		}
			break;
		case 45: {
			operation = OperationEnum.SUBTRACT;
		}
			break;
		case 46: {
			operation = OperationEnum.DECIMAL;
		}
			break;
		case 47: {
			operation = OperationEnum.DIVIDE;
		}
			break;
		case 48: {
			number = NumberEnum._0;
		}
		break;
		case 49: {
			number = NumberEnum._1;
		}
		break;
		case 50: {
			number = NumberEnum._2;
		}
		break;
		case 51: {
			number = NumberEnum._3;
		}
		break;
		case 52: {
			number = NumberEnum._4;
		}
		break;
		case 53: {
			number = NumberEnum._5;
		}
		break;
		case 54: {
			number = NumberEnum._6;
		}
		break;
		case 55: {
			number = NumberEnum._7;
		}
		break;
		case 56: {
			if (e.isShiftDown()) {
				operation = OperationEnum.MULTIPLY;
			} else {
				number = NumberEnum._8;
			}
		}
		break;
		case 57: {
			number = NumberEnum._9;
		}
		break;
		case 61: {
			if (e.isShiftDown()) {
				operation = OperationEnum.ADD;
			} else {
				operation = OperationEnum.EQUALS;
			}
		}
			break;
		case 72: {
			operation = OperationEnum.CLEAR_HISTORY;
		}
			break;
		case 73: {
			operation = OperationEnum.CLEAR_INPUT;
		}
			break;
		case 92: {
			operation = OperationEnum.DIVIDE;
		}
			break;
		}
		if (operation != null) {
			CalculatorSingleton.getInstanceof().update(operation);
			area.update();
		}
		if (number != null) {
			CalculatorSingleton.getInstanceof().update(number);
			area.update();
		}
	}
}