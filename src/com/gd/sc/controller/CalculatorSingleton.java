package com.gd.sc.controller;

import com.gd.sc.core.computation.BasicMath;
import com.gd.sc.core.computation.impl.BasicMathImpl;
import com.gd.sc.core.exception.MathOperationException;
import com.gd.sc.utils.Constant;
import com.gd.sc.utils.NumberEnum;
import com.gd.sc.utils.OperationEnum;

public class CalculatorSingleton {
	private final BasicMath math;

	private CalculatorSingleton() {
		math = new BasicMathImpl();
		history = "";
		refreash();
	}

	private void refreash() {
		setOperator(' ');
		setInput(new StringBuffer());
	}

	public static CalculatorSingleton getInstanceof() {
		return instanceOf;
	}

	private static final CalculatorSingleton instanceOf = new CalculatorSingleton();

	private String history;
	private Character operator;
	private StringBuffer input;

	public void update(NumberEnum number) {
		getInput().append(number.getNumber());
	}

	public void update(OperationEnum operator) {
		switch (operator.getOperation()) {
		case Constant.EQUAL: {
			setHistory(getResult());
			refreash();
			return;
		}
		case Constant.BACK: {
			if (getInput().length() == 1) {
				setInput(new StringBuffer(""));
				return;
			}
			if (getInput().length() < 1) {
				return;
			}
			setInput(getInput().reverse().deleteCharAt(0).reverse());
			return;
		}
		case Constant.DECIMAL: {
			if (!getInput().toString().contains("" + Constant.DECIMAL)) {
				getInput().append(Constant.DECIMAL);
			}
			return;
		}
		case Constant.CLEAR_HISTORY: {
			setHistory("");
			return;
		}
		case Constant.CLEAR_INPUT: {
			refreash();
			return;
		}
		case Constant.SIGN: {
			if (getInput().length() > 0) {
				char firstChar = getInput().charAt(0);
				if (firstChar == '-') {
					setInput(getInput().deleteCharAt(0));
				} else {
					setInput(getInput().reverse().append('-').reverse());
				}
			}
			return;
		}
		default: {
			setOperator(operator.getOperation());
			if (history.isEmpty()) {
				setHistory(input.toString());
				input = new StringBuffer("");
			}
		}
		}
	}

	private String getResult() {
		String result = "";
		if (getInput().toString().isEmpty()) {
			return getHistory();
		}
		if (getHistory().isEmpty()) {
			setHistory("0");
		}
		float h = Float.parseFloat(getHistory());
		float i = Float.parseFloat(getInput().toString());

		switch (getOperator()) {
		case Constant.ADD: {
			result = math.add(h, i).toString();
		}
		break;
		case Constant.SUBTRACT: {
			result = math.subtract(h, i).toString();
		}
		break;
		case Constant.MULTIPLY: {
			result = math.multiply(h, i).toString();
		}
		break;
		case Constant.DIVIDE: {
			try {
				result = math.divide(h, i).toString();
			} catch (MathOperationException moe) {
				result = "0";
			}
		}
		break;
		}
		return result;
	}

	public String getHistory() {
		return history;
	}

	private void setHistory(String history) {
		this.history = history;
	}

	public Character getOperator() {
		return operator;
	}

	private void setOperator(Character operator) {
		this.operator = operator;
	}

	public StringBuffer getInput() {
		return input;
	}

	private void setInput(StringBuffer input) {
		this.input = input;
	}
}
