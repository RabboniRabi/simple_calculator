package com.gd.sc.utils;

public enum OperationEnum {
	ADD(Constant.ADD), SUBTRACT(Constant.SUBTRACT), MULTIPLY(Constant.MULTIPLY), DIVIDE(Constant.DIVIDE), EQUALS(Constant.EQUAL), DECIMAL(Constant.DECIMAL), BACK(Constant.BACK), SIGN(Constant.SIGN), CLEAR_HISTORY(Constant.CLEAR_HISTORY), CLEAR_INPUT(Constant.CLEAR_INPUT);

	private final char operation;

	OperationEnum(char operation) {
		this.operation = operation;
	}

	public char getOperation() {
		return operation;
	}

	public static OperationEnum get(char operation) {
		for (OperationEnum value : values()) {
			if (value.operation == operation) {
				return value;
			}
		}
		return null;
	}
}
