package com.gd.sc.utils;

public enum NumberEnum {
	_0('0'), _1('1'), _2('2'), _3('3'), _4('4'), _5('5'), _6('6'), _7('7'), _8('8'), _9('9');
	private final char number;

	NumberEnum(char number) {
		this.number = number;
	}

	public char getNumber() {
		return number;
	}

	public static NumberEnum get(char number) {
		for (NumberEnum value : values()) {
			if (value.number == number) {
				return value;
			}
		}
		return null;
	}
}
