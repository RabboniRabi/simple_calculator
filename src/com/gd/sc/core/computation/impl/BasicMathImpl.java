package com.gd.sc.core.computation.impl;

import com.gd.sc.core.computation.BasicMath;
import com.gd.sc.core.exception.MathOperationException;
import com.gd.sc.utils.ErrorMessage;

public class BasicMathImpl implements BasicMath {

	@Override
	public Double add(float firstValue, float secondValue) {
		double result = firstValue + secondValue;
		return result;
	}

	@Override
	public Double subtract(float firstValue, float secondValue) {
		double result = firstValue - secondValue;
		return result;
	}

	@Override
	public Double multiply(float firstValue, float secondValue) {
		double result = firstValue * secondValue;
		return result;
	}

	@Override
	public Double divide(float firstValue, float secondValue) {
		if (secondValue == 0) {
			throw new MathOperationException(ErrorMessage.DIVIDE_BY_ZERO);
		}
		double result = firstValue / secondValue;
		return result;
	}

}
